package me.rustydesigns.rustycore.commands;

import me.rustydesigns.rustycore.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Staff implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd,
			String[] args) {
		
		if (cmd.equalsIgnoreCase("staff") && args.length < 0) {
			
			Player player = (Player) sender;
			
			player.sendMessage(ChatColor.RED + "Not enough arguments.");
			
		} 
		
		if (args.length > 0) {
			
			String tplayer = args[0];
			
			Main.staff.add(tplayer);
			
		}
		
		return false;
	}
	


}
