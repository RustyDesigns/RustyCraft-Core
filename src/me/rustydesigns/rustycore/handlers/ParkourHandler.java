package me.rustydesigns.rustycore.handlers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class ParkourHandler implements Listener {
	
	Location startparkour = new Location(Bukkit.getWorld("HUB"), 45, 45, 45); 
	Location spawn = new Location(Bukkit.getWorld("HUB"), 46, 46, 46);
	Location endparkour = new Location(Bukkit.getWorld("HUB"), 48, 48, 48);
	Location parkour = new Location(Bukkit.getWorld("HUB"), 44, 44, 44);
	List<String> hparkour = new ArrayList<>();
	
	@EventHandler
	public void PlayerInteract(PlayerInteractEvent event) {
		
		Player player = (Player) event.getPlayer();
		
		if (event.getAction().equals(Action.PHYSICAL) && (event.getClickedBlock().equals(Material.IRON_PLATE) && 
				(player.getLocation().equals(startparkour)))) {
			
			hparkour.add(player.getName());
			player.sendMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Rusty" + ChatColor.GRAY + "Craft] - Cave Parkour");
			player.playSound(player.getLocation(), Sound.NOTE_PLING, 100, 100);
 			
		}
		
		else if (event.getAction().equals(Action.PHYSICAL) && (event.getClickedBlock().equals(Material.GOLD_PLATE) && 
				(player.getLocation().equals(endparkour)))) {
			
			player.sendMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Rusty" + ChatColor.GRAY + "Craft] - Cave Parkour - Completed!");
			Bukkit.broadcastMessage(ChatColor.GRAY + player.getName() + " has completed the Cave Parkour!");
			
		}
		
	}
	
	@EventHandler
	public void PlayerMove(PlayerMoveEvent event) {
		
		Player player = (Player) event.getPlayer();
		
		if (hparkour.contains(player.getName()) && (player.getLocation().getBlock().equals(Material.LAVA))) {
			
			hparkour.remove(player.getName());
			player.teleport(spawn);
			
		}
		
	}

}
