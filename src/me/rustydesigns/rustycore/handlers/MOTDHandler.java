package me.rustydesigns.rustycore.handlers;

import me.rustydesigns.rustycore.Main;
import me.rustydesigns.rustycore.commands.ServerMode;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class MOTDHandler implements Listener {
	
	@EventHandler
	public static void onPing(ServerListPingEvent event) {
		
		if (ServerMode.mode.contains("Online")) {
			
			event.setMotd(Main.omotd);
			
		}
		
		if (ServerMode.mode.contains("Maintenance")) {
			
			event.setMotd(Main.mmotd);
			
		}
		
	}

}
