package me.rustydesigns.rustycore.handlers;

import me.rustydesigns.rustycore.Main;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatHandler implements Listener {
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		
		if (event.getMessage().contains("staff") || (event.getMessage().contains("Staff"))) {
			
			Player player = (Player) event.getPlayer();
			
			player.sendMessage(Main.prefix + ChatColor.WHITE + "http://www.rustycraft.us/apply");
			
			event.setCancelled(true);
		
		}
	}

}
