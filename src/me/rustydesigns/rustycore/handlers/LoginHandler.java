package me.rustydesigns.rustycore.handlers;

import me.rustydesigns.rustycore.Main;
import me.rustydesigns.rustycore.commands.ServerMode;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.permissions.PermissionAttachment;

import com.connorlinfoot.titleapi.TitleAPI;

public class LoginHandler implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		
		Player player = (Player) event.getPlayer();
		
		if (player.hasPlayedBefore()) {
		
		TitleAPI.sendTitle(player, 20, 40, 20,
				ChatColor.GOLD + "" + ChatColor.BOLD + "Welcome Back,", ChatColor.GRAY + player.getName());
		
		}
		
		if (!player.hasPlayedBefore()) {
			
			TitleAPI.sendTitle(player, 20, 40, 20,
					ChatColor.GOLD + "" + ChatColor.BOLD + "Welcome to " + ChatColor.RED + "Rusty" + ChatColor.GRAY + "Craft!",
					ChatColor.DARK_GRAY + player.getName());
			
			Bukkit.getServer().broadcastMessage(Main.prefix + ChatColor.WHITE + "Warm welcomes to " + ChatColor.GRAY + player.getName() + "!");
			
			PermissionAttachment member = player.addAttachment(new Main());
			member.setPermission("bukkit.stop", true);
			
		}
		
	}
	
	public void onPlayerLogin(PlayerLoginEvent event) {
		
		if (event.getPlayer().isBanned()) {
			
			event.getPlayer().kickPlayer(ChatColor.RED + "Sorry, you were banned, apply at: http://www.rustycraft.us/appeal");
			
		} 
		
		if (ServerMode.mode.contains("Maintenance") && (!Main.staff.contains(event.getPlayer().getName()))) {
			
			event.getPlayer().kickPlayer(ChatColor.RED + "Sorry, we are in maintenance mode, try again later.");
			
		}
		
	}
}
