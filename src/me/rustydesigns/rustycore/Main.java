package me.rustydesigns.rustycore;

import java.util.ArrayList;
import java.util.List;

import me.rustydesigns.rustycore.commands.Help;
import me.rustydesigns.rustycore.commands.ServerMode;
import me.rustydesigns.rustycore.commands.Staff;
import me.rustydesigns.rustycore.handlers.ChatHandler;
import me.rustydesigns.rustycore.handlers.FightHandler;
import me.rustydesigns.rustycore.handlers.LoginHandler;
import me.rustydesigns.rustycore.handlers.MOTDHandler;
import me.rustydesigns.rustycore.handlers.ParkourHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;

public class Main extends JavaPlugin {
	
	public static List<String> staff = new ArrayList<>();
	
	public void onEnable() {
		
		initializeholograms();
		initializeevents();
		initializecommands();
		
	}
	
	public void onDisable() {
		
		
		
	}
	
	public void initializeevents() {
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		
		pm.registerEvents(new ParkourHandler(), this);
		pm.registerEvents(new FightHandler(), this);
		pm.registerEvents(new ChatHandler(), this);
		pm.registerEvents(new LoginHandler(), this);
		pm.registerEvents(new MOTDHandler(), this);
		
		
	}
	
	public void initializecommands() {
		
		getCommand("help").setExecutor(new Help());
		getCommand("mode").setExecutor(new ServerMode());
		getCommand("staff").setExecutor(new Staff());

	}
	
	public void initializeholograms() {
		
		Location startparkour = new Location(Bukkit.getWorld("HUB"), 45, 45, 45); 
		Location endparkour = new Location(Bukkit.getWorld("HUB"), 49, 49, 49); 
		
		Hologram pks = HologramsAPI.createHologram(null, startparkour);
		ItemLine pksi1 = pks.insertItemLine(0, new ItemStack(Material.DIAMOND_BLOCK));
		TextLine pkst1 = pks.appendTextLine(ChatColor.AQUA + "Start Parkour!");
		
		pksi1.equals(ChatColor.AQUA + "Start Parkour!");
		pkst1.equals(ChatColor.AQUA + "Start Parkour!");
		
		Hologram pke = HologramsAPI.createHologram(null, endparkour);
		ItemLine pkei1 = pke.insertItemLine(0, new ItemStack(Material.GOLD_BLOCK));
		TextLine pket1 = pke.appendTextLine(ChatColor.AQUA + "Finish Parkour!");
		
		pkei1.equals(ChatColor.AQUA + "Start Parkour!");
		pket1.equals(ChatColor.AQUA + "Start Parkour!");
		
	}
	
	public static String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Rusty" + 
	ChatColor.GRAY + "Craft" + ChatColor.DARK_GRAY + "] ";
	
	public static String omotd = ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "----------[" + ChatColor.RED +
			"Rusty" + ChatColor.GRAY + "Craft" + ChatColor.GRAY + ChatColor.STRIKETHROUGH + "]----------" +
			"\n" + ChatColor.GREEN + "Were Online, come join us!";
	
	public static String mmotd = ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "----------[" + ChatColor.RED +
			"Rusty" + ChatColor.GRAY + "Craft" + ChatColor.GRAY + ChatColor.STRIKETHROUGH + "]----------" +
			"\n" + ChatColor.RED + "Maintencance Mode, try again later.";

}

