package me.rustydesigns.rustycore.handlers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class FightHandler implements Listener {

	List<String> hfight = new ArrayList<String>();
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && (event.getClickedBlock().equals(Material.DIAMOND_SWORD))
				&& (event.getPlayer().getWorld().equals(Bukkit.getWorld("HUB")))) {
			
			Player player = (Player) event.getPlayer();
			
			hfight.add(player.getName());
			player.getInventory().clear();
			ItemStack hfsword = new ItemStack(Material.DIAMOND_SWORD);
			hfsword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);
			ItemStack hfhelm = new ItemStack(Material.DIAMOND_HELMET);
			hfhelm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
			ItemStack hfchest = new ItemStack(Material.DIAMOND_CHESTPLATE);
			hfchest.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
			ItemStack hflegs = new ItemStack(Material.DIAMOND_LEGGINGS);
			hflegs.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
			ItemStack hfboots = new ItemStack(Material.DIAMOND_BOOTS);
			hfboots.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
			
			player.getInventory().setItem(1, hfsword);
			player.getInventory().setHelmet(hfhelm);
			player.getInventory().setChestplate(hfchest);
			player.getInventory().setLeggings(hflegs);
			player.getInventory().setBoots(hfboots);
			
		}
		
		else if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && (event.getClickedBlock().equals(Material.DIAMOND_SWORD))
				&& (!event.getPlayer().getWorld().equals(Bukkit.getWorld("HUB")))) {
			
			Player player = (Player) event.getPlayer();
			
			player.sendMessage(ChatColor.RED + "ERROR: Not Permitted in this world.");
			
		}
		
	}
		@EventHandler
		public void onEntityAttacked(EntityDamageByEntityEvent event) {
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			if (hfight.contains(damager.getName()) && (hfight.contains(damaged.getName()))) {
				
				
				
			}
			
			else if (hfight.contains(damager.getName()) && (!hfight.contains(damaged.getName()))) {
				
				damager.sendMessage(ChatColor.RED + "Sorry, " + damaged.getName() + " is not in the PvP mode.");
				event.setCancelled(true);
				
			}
			
			else {
				
				damaged.sendMessage(ChatColor.RED + "ERROR: Unknown Error");
				damager.sendMessage(ChatColor.RED + "ERROR: Unknown Error");
				
			}
			
		}
	
}
