package me.rustydesigns.rustycore.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ServerMode implements CommandExecutor {

	public static List<String> mode = new ArrayList<>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd,
			String[] args) {
		
		if (cmd.equalsIgnoreCase("cmd") && (args[0].equalsIgnoreCase("maintenance"))) {
			
			mode.clear();
			mode.add("Maintenance");
			
		}
		
		else if (args[0].equalsIgnoreCase("online")) {
			
			mode.clear();
			mode.add("Online");
			
		}
		
		return false;
	}

}
