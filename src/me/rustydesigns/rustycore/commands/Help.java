package me.rustydesigns.rustycore.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Help implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String cmd,
			String[] args) {
		
		Player player = (Player) sender;
		
		if (cmd.equalsIgnoreCase("help")) {
			
			player.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "----------[" + ChatColor.RED +
					"Rusty" + ChatColor.GRAY + "Craft" + ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "]----------");
			player.sendMessage(ChatColor.GRAY + "/help - this command. (Server: All)");
			player.sendMessage(ChatColor.GRAY + "/f - main factions command. (Server: Factions)");
			player.sendMessage(ChatColor.GRAY + "/p - main plots command. (Server: Creative)");
			player.sendMessage(ChatColor.GRAY + "/kit - main kits command. (Server: Factions, KitPvP)");
			player.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "----------[" + ChatColor.RED +
					"Rusty" + ChatColor.GRAY + "Craft" + ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "]----------");
			
		}
		
		return false;
		
	}
	
}
